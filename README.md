# guv

Update as of 2021-08-04

Project Pivot:  After spending a lot of time looking at making a wayland compositor in golang; I realized I would rather work on an open source e-ink system based on linux.

This is because of the following

1) I want a linux based A4 or A5 e-ink reader
2) That is linux not android
3) That isn't locked down
4) The typography stuff is interesting.
5) Ebook Reader
6) Audio Book player (audio is also interesting)

## Current state

`bin/guv` contains the testing/example binary as I spike out each system.  Currently handles poling input and png output; which
once I get a few more pieces I can wire out to a screen and start interacting with an e-paper display.

## Immediate TODO

- [ ] find an eink display to work with 

- [x] Breakdown architecture for components (state, storage, text render, layout, server/client)
  - ECS system; i have input and render sketched out.  binary in bin/guv
  - going to use github.com/tdewolff/canvas as the rendering target (basically its cairo but built in golang so
    its easier to use than any of the wrapper libraries, it doesn't have to use CGO, and it implements' knuth's
    text wrapping algo already!

- [x] Chose base
  - Initial hardware is RPI 4 compute + io board.  I'd rather use something RISC-V; and probably even lower powered.  The PocketBeagle looks like a good target after i get everything working
  - I want a full linux install.
- [ ] Audio out?
  - Waiting on hardware; initially this is RPI compute.  Will need to solve an interface; player and the audio stack.
  - Want to support wired and bluetooth; but I have limited bluetooth headphones to test.

This is a complete pivot on what guv was originally, but i'm actually likely to use this; where the compositor idea was more a learning project.  The work done on the layout algorithm for guv is still applicable as it was designed to be super light weight and low refresh.  Some of the more complex layout stuff can be dropped.

The typography, mobi, epub and pdf stuff is going to be most complex part; and I'm tempted to go with TeX/dvi based approaches as a start; because the mode of reading is much closer to print than screen.

## Research

Its going to be in the [wiki](https://gitlab.com/yarbelk/guv/-/wikis/home) now.
