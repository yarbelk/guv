package main

import (
	"context"
	"fmt"
	"image"
	"os"
	"sync"

	"image/color"

	"gitlab.com/repub/guv"
	"gitlab.com/repub/guv/errors"
	"gitlab.com/repub/guv/systems/input"
	"gitlab.com/repub/guv/systems/render"
)

// Colors here are for testing
var (
	Red    = color.RGBA{255, 0, 0, 255}
	Green  = color.RGBA{0, 255, 0, 255}
	Blue   = color.RGBA{0, 0, 255, 255}
	Yellow = color.RGBA{255, 255, 0, 255}
)

// printKey is for testing.
func printKey(i interface{}) {
	if e, ok := i.(input.IOEvent); ok {
		fmt.Printf("%s\n\r", e.Name)
	}
}

// setColor is for testing purposes.
func setColor(rc *render.RenderComponent, sf *render.SolidFill, c color.Color) func(interface{}) {
	return func(interface{}) {
		rc.Dirty = true
		sf.Color = c
	}
}

func main() {
	// This is a testing app: the systems as implemented here are the minimum i need to get
	// some functionality implemented so I can understand the problem space bette.r
	// Directly writing to files, reusing the same FD.  using basic rectangles of colors
	// and text output echoing keys pressed are _not_ the end goal.
	// I intend to replace the render system with a hierarchical layout engine
	// built with github.com/tdewolff/canvas
	// the majority of the setup can then be done data driven as this is an ECS system
	ctx, cancel := context.WithCancel(context.Background())
	wg := sync.WaitGroup{}
	triggerChan := make(chan guv.Event)

	exitFunc := func(i interface{}) {
		cancel()
	}

	fd, err := os.Create("output.png")
	if err != nil {
		panic(err)
	}

	screenSize := image.Rectangle{image.Point{0, 0}, image.Point{100, 100}}

	img := image.NewRGBA(screenSize)

	renderSystem := render.New(ctx, nil, img, render.NewFileWriter(fd))

	topLevelDrawer := &render.SolidFill{Color: Blue}
	topLevel := render.RenderComponent{
		Offset: image.Point{10, 10},
		Size:   image.Point{80, 80},
		Drawer: topLevelDrawer,
		ZIndex: 1,
	}

	renderSystem.Add(guv.NewBaseEntity(), &topLevel)

	inputSystem := input.New(ctx, &wg)
	inputSystem.Add(
		guv.NewBaseEntity(),
		&input.KeyComponent{Key: input.KeyCtrlC},
		&input.ActionComponent{Action: printKey},
	)
	inputSystem.Add(
		guv.NewBaseEntity(),
		&input.KeyComponent{Key: input.KeyEsc},
		&input.ActionComponent{Action: printKey},
	)
	inputSystem.Add(
		guv.NewBaseEntity(),
		&input.KeyComponent{Key: input.KeyCtrlC},
		&input.ActionComponent{Action: exitFunc},
	)
	inputSystem.Add(
		guv.NewBaseEntity(),
		&input.KeyComponent{Key: input.KeyEsc},
		&input.ActionComponent{Action: exitFunc},
	)
	// This stuff can be moved out later when i have more things to set up.  its entirely possible to do this as
	// data (eg; json or yaml definitions).  IDs should be automated too
	inputSystem.Add(
		guv.NewBaseEntity(),
		&input.KeyComponent{Key: input.KeyArrowLeft},
		&input.ActionComponent{Action: printKey},
	)
	inputSystem.Add(
		guv.NewBaseEntity(),
		&input.KeyComponent{Key: input.KeyArrowLeft},
		&input.ActionComponent{Action: setColor(&topLevel, topLevelDrawer, Blue)},
	)
	inputSystem.Add(
		guv.NewBaseEntity(),
		&input.KeyComponent{Key: input.KeyArrowRight},
		&input.ActionComponent{Action: printKey},
	)
	inputSystem.Add(
		guv.NewBaseEntity(),
		&input.KeyComponent{Key: input.KeyArrowRight},
		&input.ActionComponent{Action: setColor(&topLevel, topLevelDrawer, Green)},
	)
	inputSystem.Add(
		guv.NewBaseEntity(),
		&input.KeyComponent{Key: input.KeyArrowUp},
		&input.ActionComponent{Action: printKey},
	)
	inputSystem.Add(
		guv.NewBaseEntity(),
		&input.KeyComponent{Key: input.KeyArrowUp},
		&input.ActionComponent{Action: setColor(&topLevel, topLevelDrawer, Red)},
	)
	inputSystem.Add(
		guv.NewBaseEntity(),
		&input.KeyComponent{Key: input.KeyArrowDown},
		&input.ActionComponent{Action: printKey},
	)
	inputSystem.Add(
		guv.NewBaseEntity(),
		&input.KeyComponent{Key: input.KeyArrowDown},
		&input.ActionComponent{Action: setColor(&topLevel, topLevelDrawer, Yellow)},
	)

	// TODO Switch this to unicode/keyboard map instead.  this is simple version for PoC
	var i byte
	for i = 32; i < 127; i++ {
		s := string([]byte{i})
		inputSystem.Add(
			guv.NewBaseEntity(),
			&input.KeyComponent{Key: s},
			&input.ActionComponent{Action: printKey},
		)

	}
	g := guv.New(ctx)
	g.AddSystem(inputSystem)
	g.AddSystem(renderSystem)
	g.AddEventChan(triggerChan)
	g.AddEventHandler("IOEvent", func(e guv.Event) {})
	go inputSystem.WatchKeys(ctx, triggerChan)
	err = g.EventLoop()
	if err == nil || err == errors.EventLoopClosed {

		wg.Wait()
		os.Exit(0)
	}

	// TODO replace with guv event loop and print actions
	wg.Wait()
	os.Exit(1)
}
