package errors

// SentinalError are thoses errors that don't
// have details or never change.  like io.EOF should
// have been.
type SentinalError string

const (
	// EventLoopClosed is for when the event loop channel was closed
	EventLoopClosed SentinalError = "event loop closed"
)

func (se SentinalError) Error() string {
	return string(se)
}
