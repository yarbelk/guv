package guv

// Event types can trigger things on Guv
type Event interface {
	Type() string
}

// UpdateTrigger is an event type that will trigger an
// update
type UpdateTrigger interface {
	Update()
}
