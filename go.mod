module gitlab.com/repub/guv

go 1.12

require (
	github.com/tdewolff/canvas v0.0.0-20210809125206-89ef45200a5c
	golang.org/x/sys v0.0.0-20210806184541-e5e7981a1069 // indirect
	golang.org/x/term v0.0.0-20210615171337-6886f2dfbf5b
)
