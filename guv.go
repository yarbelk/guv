package guv

import (
	"context"
	"fmt"
	"log"
	"sync"
	"time"

	"gitlab.com/repub/guv/errors"
)

// Guv is the main app for the program.
type Guv struct {
	systems []System

	ctx           context.Context
	eventFanIn    chan Event
	lastUpdate    time.Time
	eventHandlers map[string][]func(Event)

	*sync.RWMutex
}

// New empty Guv
func New(ctx context.Context) *Guv {
	return &Guv{
		RWMutex:       &sync.RWMutex{},
		ctx:           ctx,
		lastUpdate:    time.Now(),
		eventHandlers: make(map[string][]func(Event)),
		eventFanIn:    make(chan Event),
	}
}

// Update each system.  This is called on event; unlike with most ECS systems, frames may be
// far between, so it is triggered by various other things like inputs and timers.
func (g *Guv) Update(dt time.Duration) {
	for _, system := range g.systems {
		system.Update(dt)
	}
}

// AddSystem will uniquely add a system.  its a panic if its not
// unique
func (g *Guv) AddSystem(s System) {
	for _, t := range g.systems {
		if t.Name() == s.Name() {
			panic(fmt.Sprintf("already added system %s %s: %v", t.Name(), s.Name(), s))
		}
	}
	g.systems = append(g.systems, s)
}

// AddEventChan fans in all events to the Guv app for processing
// its a FIFO in as much as a co-processing multi threaded thing
// can be (eg: no priorities)
func (g *Guv) AddEventChan(in <-chan Event) {
	go func() {
		for {
			select {
			case e, ok := <-in:
				if !ok {
					return
				}
				g.eventFanIn <- e
			case <-g.ctx.Done():
				return
			}
		}
	}()
}

// AddEventHandler to the event loop.
func (g *Guv) AddEventHandler(t string, h func(e Event)) {
	g.RWMutex.Lock()
	defer g.RWMutex.Unlock()
	var ok bool

	if g.eventHandlers == nil {
		g.eventHandlers = make(map[string][]func(Event))
	}

	if _, ok = g.eventHandlers[t]; !ok {
		g.eventHandlers[t] = make([]func(Event), 0)
	}
	g.eventHandlers[t] = append(g.eventHandlers[t], h)
}

// EventLoop triggers only on passed messages.  If you want
// If the events are UpdateTrigger events, this will trigger
// an update.  All events look for a handler, and will log on
// orphaned event types
func (g *Guv) EventLoop() error {
	for {
		if g.ctx.Err() != nil {
			return nil
		}
		e, ok := <-g.eventFanIn
		if !ok {
			// may be because of race condition
			// after closing the context; but if it isn't
			// this is a real error
			return errors.EventLoopClosed
		}
		if _, ok := e.(UpdateTrigger); ok {
			g.Update(time.Now().Sub(g.lastUpdate))
		}

		if g.eventHandlers == nil {
			continue
		}
		// not unlocked with defer; see below
		g.RWMutex.RLock()
		if hs, ok := g.eventHandlers[e.Type()]; ok {
			for _, handler := range hs {
				handler(e)
			}
		} else {
			log.Println("Orphan Event:", e.Type())
		}
		g.RWMutex.RUnlock()

	}

}
