package render

import (
	"context"
	"fmt"
	"image"
	"image/draw"
	"image/png"
	"io"
	"os"
	"sort"
	"time"

	"github.com/tdewolff/canvas"
	"gitlab.com/repub/guv"
)

type Point = image.Point

// ALl these are liable to move to new places as I add more stuff

// Drawer needs to either wrap go's draw.Drawer or canvas's canvas.Renderer or Context
type Drawer interface {
	Draw(img draw.Image)
}

// FileWriter manages resetting, sizing and truncating the single underlying file.
// this is probably silly now that i write this
type FileWriter struct {
	out *os.File

	encoder png.Encoder
}

// NewFileWriter for png at no compression
func NewFileWriter(out *os.File) FileWriter {
	return FileWriter{
		out:     out,
		encoder: png.Encoder{CompressionLevel: png.NoCompression},
	}
}

// Close the underliying file
func (fw *FileWriter) Close() error {
	return fw.out.Close()
}

// Encode to
func (fw *FileWriter) Encode(m image.Image) error {
	size, err := fw.out.Seek(0, io.SeekEnd)
	if err != nil {
		return err
	}
	fw.out.Seek(0, io.SeekStart)
	if err = fw.encoder.Encode(fw.out, m); err != nil {
		return err
	}
	if err = fw.out.Sync(); err != nil {
		return err
	}
	pos, err := fw.out.Seek(0, io.SeekCurrent)
	if size > pos {
		return fw.out.Truncate(pos)
	}
	return nil
}

// RenderComponent abstracts a subsurface (which may be an entire surface) and layer.
// Its currently missing semantics about masks etc; which may be useful in the future
// for sub-region updates to screens. (OR all your dirty masks; break down which
// regions these impact, only write out those changes to your screen)
type RenderComponent struct {
	// Dirty controls if a component is re-rendered or not
	Dirty bool
	// Hidden controls if anything is done on the render pass.  since this is a
	// really basic render each layer sequentially (with dirty and hiden of course)
	// this just means "don't do anything when updated" which keeps the underlying
	// rendered stuff the same.  this means the entire tree of ancestors need to be marked
	// dirty in a region.
	Hidden bool
	// Offset from parent origin.  Currently coded to canvas origin
	Offset Point
	// Size of region
	Size Point

	// Drawer contains the logic of what to blit to the region
	Drawer Drawer
	// img is underlying image resource.  equiv of a glx texture or whatever.
	// since everything is going to be written serially out to some e-paper screen;
	// we just using this.
	img draw.Image

	// ZIndex for this entity.  Higher is top
	ZIndex int32
}

// SetImage to new one.  this doesn't draw; just change the underlying canvas
func (rc *RenderComponent) SetImage(img draw.Image) {
	rc.img = img
}

// Render the stuff in the Drawer to the internal buffer
func (rc *RenderComponent) Render() {
	if rc.Hidden {
		return
	}
	if rc.Dirty {
		rc.Drawer.Draw(rc.img)
		rc.Dirty = false
	}
	return
}

// Entity contains the render component machinery for the RenderSystem to do its job.
type Entity struct {
	guv.BaseEntity
	*RenderComponent
}

type entities []Entity

func (es entities) Len() int { return len(es) }
func (es entities) Less(i, j int) bool {
	return es[i].RenderComponent.ZIndex < es[i].RenderComponent.ZIndex
}
func (es entities) Swap(i, j int) { es[i], es[j] = es[j], es[i] }

func New(ctx context.Context, canvas *canvas.Canvas, img draw.Image, out FileWriter) *RenderSystem {
	return &RenderSystem{
		canvas:   canvas,
		img:      img,
		ctx:      ctx,
		out:      out,
		entities: make(entities, 0),
	}
}

// RenderSystem will blit the entities to a surface
type RenderSystem struct {
	canvas    *canvas.Canvas
	canvasCtx canvas.Context
	img       draw.Image
	ctx       context.Context

	out FileWriter

	entities entities
}

// Context givws you a canvas context.  Don't know why yet
func (rs *RenderSystem) Context() canvas.Context {
	return rs.canvasCtx
}

// subImage with valid draw.Image interface
func (rs *RenderSystem) subImage(r image.Rectangle) (img draw.Image, err error) {
	switch i := rs.img.(type) {
	case *image.Alpha:
		img = i.SubImage(r).(*image.Alpha)
	case *image.Alpha16:
		img = i.SubImage(r).(*image.Alpha16)
	case *image.CMYK:
		img = i.SubImage(r).(*image.CMYK)
	case *image.Gray:
		img = i.SubImage(r).(*image.Gray)
	case *image.Gray16:
		img = i.SubImage(r).(*image.Gray16)
	case *image.NRGBA:
		img = i.SubImage(r).(*image.NRGBA)
	case *image.NRGBA64:
		img = i.SubImage(r).(*image.NRGBA64)
	case *image.Paletted:
		img = i.SubImage(r).(*image.Paletted)
	case *image.RGBA:
		img = i.SubImage(r).(*image.RGBA)
	case *image.RGBA64:
		img = i.SubImage(r).(*image.RGBA64)
	default:
		img, err = nil, fmt.Errorf("unknown image type")
	}
	return
}

// Add a new render.Entity into the system.  stored sorted by zindex
func (rs *RenderSystem) Add(be guv.BaseEntity, rc *RenderComponent) {
	r := image.Rectangle{Min: rc.Offset, Max: rc.Size} // Not sure of order: this is coordinate centric
	si, err := rs.subImage(r)
	if err != nil {
		panic(err)
	}
	rc.SetImage(si)
	rc.Dirty = true
	rs.entities = append(rs.entities, Entity{BaseEntity: be, RenderComponent: rc})
	// lazy and slow way of sorting.  Esp since its already sorted.
	sort.Sort(rs.entities)
}

// Update the entire image
func (rs *RenderSystem) Update(dt time.Duration) error {
	for _, rc := range rs.entities {
		rc.Render()
	}
	return rs.out.Encode(rs.img)
}

func (rs *RenderSystem) Remove(be guv.BaseEntity) {}
func (rs *RenderSystem) Name() string             { return "RenderSystem" }
